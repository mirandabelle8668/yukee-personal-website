import React from "react";
import "../yukee-background.mp4"


class Home extends React.Component{

    render() {
        const videoSource = "yukee-background.mp4";
        return (
            <div>
                <video autoPlay = "autoPlay" muted poster="../first-pic.png" id="bgvid">
                    <source src={videoSource} type="video/mp4"/>
                </video>
            </div>
        )
    }
}

export default Home