import React from 'react';
import './components/home'
import Home from "./components/home";

function App() {
  return (
    <div className="App">
      <Home></Home>
    </div>
  );
}

export default App;
